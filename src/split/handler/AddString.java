package split.handler;

import java.util.List;

public class AddString implements Handler{
    private String string;

    public AddString(String _string){
        string = _string;
    }

    @Override
    public void handle(List<String> list) {
        list.set(list.size()-1, list.get(list.size()-1) + string);
    }
}
