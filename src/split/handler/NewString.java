package split.handler;

import java.util.List;

public class NewString implements Handler {
    private String string;

    public NewString(String _string){
        string = _string;
    }

    @Override
    public void handle(List<String> list) {
        list.add(string);
    }
}
