package split.handler;

import java.util.List;

public interface Handler {
    void handle(List<String> list);
}
