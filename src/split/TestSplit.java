package split;

import abstract_automaton.ExceptionFSM;
import abstract_test.Test;

import java.util.ArrayList;
import java.util.List;

public class TestSplit {

    public static void main(String[] args) {
        List<Test> tests = new ArrayList<>();

        tests.add(new Test("Test1Empty") {
            public boolean test() {
                boolean result;
                try {
                    result = Split.split("","").size() == 0;
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("Test2Empty") {
            public boolean test() {
                boolean result;
                try {
                    List<String> result_list = Split.split("hallo world","");
                    result = result_list.get(0).equals("hallo world");
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestCharacter") {
            public boolean test() {
                boolean result;
                try {
                    List<String> result_list = Split.split("hallo  world"," ");
                    result = result_list.size() == 2 && result_list.get(0).equals("hallo") && result_list.get(1).equals("world");
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestString") {
            public boolean test() {
                boolean result;
                try {
                    String value = "Эм... Конечный автомат — это абстрактный автомат." +
                            "Эм... У него число возможных внутренних состояний которого конечно.";
                    String edit_value = "Конечный автомат — это абстрактный автомат." +
                            "У него число возможных внутренних состояний которого конечно.";
                    String trash = "Эм... ";
                    List<String> result_list = Split.split(value,trash);
                    StringBuilder message = new StringBuilder();
                    for (String string: result_list) {
                        message.append(string);
                    }
                    result = message.toString().equals(edit_value);
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });


        for (Test test: tests) {
            System.out.println(test);
        }
    }
}
