package split;

import abstract_automaton.ExceptionFSM;
import abstract_automaton.FSM;
import split.handler.AddString;
import split.handler.Handler;
import split.handler.NewString;
import split.handler.Nothing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Split {

    private static FSM<Character, ?, Handler> createFSM(Set<Character> alphabet, String separation){
        FSM<Character, Integer, Handler> automaton = new FSM<>();
        automaton.addStartState(0);
        for(Character ch: alphabet){
            automaton.addNewTransition(0,ch,1, new NewString(ch.toString()));
            for(int i=0; i<Math.max(1,separation.length());++i){
                automaton.addNewTransition(i+1,ch,1,
                        new AddString(separation.substring(0,i) + ch.toString()));
            }
        }
        for (int i=0; i<separation.length()-1;++i){
            automaton.addNewTransition(i+1,separation.charAt(i),i+2,new Nothing());
        }
        try{
            automaton.addNewTransition(0,separation.charAt(0),Math.min((separation.length()-1)*2,2),new Nothing());
            automaton.addNewTransition(separation.length(),separation.charAt(separation.length()-1),
                    0,new Nothing());
        }catch (StringIndexOutOfBoundsException ignored){}
        return automaton;
    }

    public static List<String> split(String text, String separation) throws ExceptionFSM {
        Set<Character> alphabet = new HashSet<>();
        Character[] arguments = new Character[text.length()];
        for (int i=0; i<text.length();++i){
            alphabet.add(text.charAt(i));
            arguments[i] = text.charAt(i);
        }
        for (int i=0; i<separation.length();++i){
            alphabet.add(text.charAt(i));
        }
        FSM<Character, ?, Handler> automaton = createFSM(alphabet,separation);
        List<String> result = new ArrayList<>();
        List<Handler> list = automaton.readCommands(arguments);
        for (Handler handler: list){
            handler.handle(result);
        }
        return result;
    }
}
