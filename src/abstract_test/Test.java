package abstract_test;

public abstract class Test {
    private String name;

    public Test(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract boolean test();

    public String toString() {
        return this.getName() + ": " + this.test();
    }
}
