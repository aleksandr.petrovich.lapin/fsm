package markdown;

import abstract_automaton.ExceptionFSM;
import abstract_test.Test;

import java.util.ArrayList;
import java.util.List;

public class MarkdownTest {

    public static void main(String[] args) {
        List<Test> tests = new ArrayList<>();

        tests.add(new Test("TestEnum") {
            public boolean test() {
                boolean result;
                try {
                    result = Markdown.builder(new MarkdownConfig()).count("").length() == 0;
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestText") {
            public boolean test() {
                boolean result;
                try {
                    Markdown mark_down = Markdown.builder(new MarkdownConfig());
                    String text = "Hello\nMy name is Aleksandr\n";
                    result = mark_down.count(text).equals(text);
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestGrating") {
            public boolean test() {
                boolean result;
                try {
                    Markdown mark_down = Markdown.builder(new MarkdownConfig());
                    String value1 = "#Hello";
                    String result1 = "<h1>Hello</h1>";
                    String value2 = "#H1\n##H2\ntext";
                    String result2 = "<h1>H1</h1>\n<h2>H2</h2>\ntext";
                    result = mark_down.count(value1).equals(result1) && mark_down.count(value2).equals(result2);
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestStar") {
            public boolean test() {
                boolean result;
                try {
                    Markdown mark_down = Markdown.builder(new MarkdownConfig());
                    String value1 = "*Hello*";
                    String result1 = "<i>Hello</i>";
                    String value2 = "**b**";
                    String result2 = "<b>b</b>";
                    result = mark_down.count(value1).equals(result1) && mark_down.count(value2).equals(result2);
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestList") {
            public boolean test() {
                boolean result;
                try {
                    Markdown mark_down = Markdown.builder(new MarkdownConfig());
                    String value1 = "-1\n-2";
                    String result1 = "<ul>\n<li>1</li>\n<li>2</li>\n</ul>\n";
                    String value2 = "-1\ntext";
                    String result2 = "<ul>\n<li>1</li>\n</ul>\ntext";
                    result = mark_down.count(value1).equals(result1) && mark_down.count(value2).equals(result2);
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });
        tests.add(new Test("TestQuote") {
            public boolean test() {
                boolean result;
                try {
                    Markdown mark_down = Markdown.builder(new MarkdownConfig());
                    String value1 = ">1";
                    String result1 = "<blockquote>1</blockquote>";
                    String value2 = ">1\ntext";
                    String result2 = "<blockquote>1</blockquote>\ntext";
                    result = mark_down.count(value1).equals(result1) && mark_down.count(value2).equals(result2);
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                }
                return result;
            }
        });

        for (Test test: tests) {
            System.out.println(test);
        }
    }
}
