package markdown.counter;

import markdown.Hoarder;

public class AddChar implements Counter {

    private char ch;

    public AddChar(Character _ch){
        ch = _ch;
    }

    @Override
    public void count(Hoarder hoarder) {
        hoarder.addChar(ch);
    }
}

