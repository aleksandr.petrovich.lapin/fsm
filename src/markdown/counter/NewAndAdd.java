package markdown.counter;

import markdown.Hoarder;
import markdown.typer.Typist;

public class NewAndAdd implements Counter{

    private Typist typist;
    private char ch;

    public NewAndAdd(Typist _typist, char _ch){
        typist = _typist;
        ch = _ch;
    }

    @Override
    public void count(Hoarder hoarder) {
        hoarder.addTypist(typist);
        hoarder.addChar(ch);
    }
}
