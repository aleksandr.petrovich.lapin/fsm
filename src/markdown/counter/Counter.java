package markdown.counter;

import markdown.Hoarder;

public interface Counter {
    void count(Hoarder hoarder);
}
