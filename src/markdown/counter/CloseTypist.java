package markdown.counter;

import markdown.Hoarder;

public class CloseTypist implements Counter {

    private char ch;

    public CloseTypist(Character _ch){
        ch = _ch;
    }

    @Override
    public void count(Hoarder hoarder) {
        hoarder.closeTypist(ch);
    }
}
