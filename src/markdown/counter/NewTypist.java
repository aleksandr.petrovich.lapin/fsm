package markdown.counter;

import markdown.Hoarder;
import markdown.typer.Typist;

public class NewTypist implements Counter{

    private Typist typist;


    public NewTypist(Typist _typist){
        typist = _typist;
    }

    @Override
    public void count(Hoarder hoarder) {
        hoarder.addTypist(typist);
    }
}