package markdown.counter;

import markdown.Hoarder;

public class UpdateTypist implements Counter {

    private char ch;

    public UpdateTypist(Character _ch){
        ch = _ch;
    }

    @Override
    public void count(Hoarder hoarder) {
        hoarder.updateTypist(ch);
    }
}
