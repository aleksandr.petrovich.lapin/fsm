package markdown.builder_counter;

import markdown.BuilderCounters;
import markdown.counter.Counter;
import markdown.counter.NewTypist;

public class BuilderNewTypist implements BuilderCounter {
    @Override
    public Counter create(String[] argument) {
        return new NewTypist(BuilderCounters.getTypist(argument[1]));
    }
}