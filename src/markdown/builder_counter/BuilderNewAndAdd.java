package markdown.builder_counter;

import markdown.BuilderCounters;
import markdown.counter.Counter;
import markdown.counter.NewAndAdd;

public class BuilderNewAndAdd implements BuilderCounter {
    @Override
    public Counter create(String[] argument) {
        return new NewAndAdd(BuilderCounters.getTypist(argument[1]),argument[2].charAt(0));
    }
}
