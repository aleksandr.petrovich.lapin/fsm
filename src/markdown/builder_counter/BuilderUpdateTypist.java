package markdown.builder_counter;

import markdown.counter.Counter;
import markdown.counter.UpdateTypist;

public class BuilderUpdateTypist implements BuilderCounter {
    @Override
    public Counter create(String[] argument) {
        return new UpdateTypist(argument[1].charAt(0));
    }
}
