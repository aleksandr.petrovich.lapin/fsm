package markdown.builder_counter;

import markdown.counter.CloseTypist;
import markdown.counter.Counter;

public class BuilderCloseTypist implements BuilderCounter {
    @Override
    public Counter create(String[] argument) {
        return new CloseTypist(argument[1].charAt(0));
    }
}
