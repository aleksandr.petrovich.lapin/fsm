package markdown.builder_counter;

import markdown.counter.Counter;

public interface BuilderCounter {
    Counter create(String[] argument);
}
