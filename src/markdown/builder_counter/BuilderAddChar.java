package markdown.builder_counter;

import markdown.counter.AddChar;
import markdown.counter.Counter;

public class BuilderAddChar implements BuilderCounter {

    @Override
    public Counter create(String[] argument) {
        return new AddChar(argument[1].charAt(0));
    }
}

