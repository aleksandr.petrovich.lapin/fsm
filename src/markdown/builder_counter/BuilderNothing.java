package markdown.builder_counter;

import markdown.counter.Counter;
import markdown.counter.Nothing;

public class BuilderNothing implements BuilderCounter {

    @Override
    public Counter create(String[] argument) {
        return new Nothing();
    }
}
