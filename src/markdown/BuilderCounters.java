package markdown;

import markdown.builder_counter.*;
import markdown.counter.Counter;
import markdown.typer.Quote;
import markdown.typer.Text;
import markdown.typer.Typist;
import markdown.typer.TypistList;
import markdown.typer.grating.Grating_Text;
import markdown.typer.star.Star_Text;

import java.util.HashMap;
import java.util.Map;

public class BuilderCounters {

    public static Typist getTypist(String argument){
        Map<String, Typist> typist_map = new HashMap<>();
        typist_map.put("Text", new Text());
        typist_map.put("Quote", new Quote());
        typist_map.put("Grating_Text", new Grating_Text());
        typist_map.put("Star_Text", new Star_Text());
        typist_map.put("TypistList", new TypistList());
        return typist_map.get(argument);
    }

    public static Counter createCounter(String value){
        Map<String, BuilderCounter> builder_map = new HashMap<>();
        builder_map.put("AddChar", new BuilderAddChar());
        builder_map.put("CloseTypist", new BuilderCloseTypist());
        builder_map.put("NewAndAdd", new BuilderNewAndAdd());
        builder_map.put("NewTypist", new BuilderNewTypist());
        builder_map.put("Nothing", new BuilderNothing());
        builder_map.put("UpdateTypist", new BuilderUpdateTypist());

        String[] arguments = value.split("~");

        return builder_map.get(arguments[0]).create(arguments);
    }
}
