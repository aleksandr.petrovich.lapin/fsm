package markdown;

import abstract_automaton.ExceptionFSM;
import abstract_automaton.FSM;
import markdown.counter.Counter;

import java.util.List;
import java.util.Map;

public class Markdown {
    private FSM<Character,Integer, Counter> fsm;

    private Markdown(Map<Integer, Map<Character, Integer>> transition_matrix,
                    Map<Integer, Map<Character, Counter>> output_matrix){
        fsm = new FSM<>(transition_matrix, output_matrix);
    }

    public static Markdown builder(MarkdownConfig markdown_config){
        return new Markdown(markdown_config.getTransitionMatrix(), markdown_config.getOutputMatrix());
    }

    public String count(String string) throws ExceptionFSM {
        fsm.addStartState(0);
        Character args[]= new Character[string.length()];
        for (int i=0; i<string.length();++i){
            args[i] = string.charAt(i);
        }
        List<Counter> list = fsm.readCommands(args);
        Hoarder hoarder = new Hoarder();
        for (Counter counter: list){
            counter.count(hoarder);
        }
        return hoarder.getResult();
    }
}
