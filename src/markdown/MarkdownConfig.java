package markdown;

import markdown.counter.Counter;

import java.util.HashMap;
import java.util.Map;

public class MarkdownConfig {
    private Map<Integer, Map<Character, Integer>> transition_matrix;
    private Map<Integer, Map<Character, String>> output_matrix;

    public MarkdownConfig(){
        transition_matrix = new HashMap<>();
        output_matrix = new HashMap<>();

        String alphabet_string= "123456789!@#$:;%^&?*_-+=/|\\\"\n <>[]{}()'"+
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUYWXYZ" +
                "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЁКЛМНОПРСТУФХЦЧЪЫЬЭЮЯ";
        char alphabet[] = alphabet_string.toCharArray();
        for (char ch: alphabet){//если нет специального знака, то вводиться просто текст
            addNewTransition(0,ch,1, "NewAndAdd~Text~"+ch); //new NewAndAdd(new Text(),ch))
        }
        addNewTransition(0,'\n',0,"NewAndAdd~Text~\n");//пропуст строки new NewAndAdd(new Text(), '\n')
        for (char ch: alphabet){//читаем текст
            addNewTransition(1,ch,1,"AddChar~" + ch); //new AddChar(ch)
        }
        addNewTransition(1,'\n',0, "NewAndAdd~Text~\n");//заканчиваем читать текст new NewAndAdd(new Text(),'\n')
        addNewTransition(1,'*',1,"CloseTypist~*");//акрываем звездочки new CloseTypist('*')

        addNewTransition(0,'>',7,"NewTypist~Quote");//> new NewTypist(new Quote())
        for (char ch: alphabet){//#Text
            addNewTransition(7,ch,1,"AddChar~" + ch); //new AddChar(ch)
        }
        addNewTransition(7,'\n',0,"NewAndAdd~Text~\n");//>\n new NewAndAdd(new Text(),'\n')

        addNewTransition(0,'#',2,"NewTypist~Grating_Text");//# new NewTypist(new Grating_Text())
        for (char ch: alphabet){//#Text
            addNewTransition(2,ch,1,"AddChar~" + ch); //new AddChar(ch)
        }
        addNewTransition(2,'\n',0,"NewAndAdd~Text~\n");//#\n new NewAndAdd(new Text(),'\n')
        addNewTransition(2,'#',3,"UpdateTypist~#");//## new UpdateTypist('#')
        for (char ch: alphabet){//##Text
            addNewTransition(3,ch,1,"AddChar~" + ch); //new AddChar(ch)
        }
        addNewTransition(3,'\n',0,"NewAndAdd~Text~\n");//##\n new NewAndAdd(new Text(),'\n')

        addNewTransition(0,'*',4, "NewTypist~Star_Text");//* new NewTypist(new Star_Text())
        for (char ch: alphabet){//*Text
            addNewTransition(4,ch,1,"AddChar~" + ch); //new AddChar(ch)
        }
        addNewTransition(4,'\n',0,"NewAndAdd~Text~\n");//*\n new NewAndAdd(new Text(),'\n')
        addNewTransition(4,'*',1,"UpdateTypist~*");//** new UpdateTypist('*')

        addNewTransition(0,'-',5,"NewTypist~TypistList");//- new NewTypist(new TypistList())
        for (char ch: alphabet){//-Text
            addNewTransition(5,ch,5,"AddChar~" + ch); //new AddChar(ch)
        }
        addNewTransition(5,'\n',6,"Nothing"); // new Nothing()
        for (char ch: alphabet){//-Text\nText
            addNewTransition(6,ch,2,"NewAndAdd~Text~" + ch); // new NewAndAdd(new Text(),ch)
        }
        addNewTransition(6,'#',2, "NewTypist~GratingText");//-Text\n# new NewTypist(new Grating_Text())
        addNewTransition(6,'*',4,"NewTypist~Star_Text");//-Text\n* new NewTypist(new Star_Text())
        addNewTransition(6,'>',7,"NewTypist~Quote");//-Text\n> new NewTypist(new Quote())
        addNewTransition(6,'\n',0,"NewAndAdd~Text~\n"); // new NewAndAdd(new Text(),'\n')
        addNewTransition(6,'-',5,"UpdateTypist~\n"); // new UpdateTypist('\n')
    }

    public Map<Integer, Map<Character, Integer>> getTransitionMatrix(){
        return transition_matrix;
    }

    public Map<Integer, Map<Character, Counter>> getOutputMatrix(){
        Map<Integer, Map<Character, Counter>> result = new HashMap<>();
        for (Integer key1: output_matrix.keySet()){
            Map<Character, String> output = output_matrix.get(key1);
            Map<Character, Counter> element_result = new HashMap<>();
            for (Character key2: output.keySet()){
                element_result.put(key2, BuilderCounters.createCounter(output.get(key2)));
            }
            result.put(key1, element_result);
        }
        return result;
    }

    private void addNewTransition(Integer state, Character key, Integer new_state, String result){
        Map<Character,Integer> transitions = transition_matrix.getOrDefault(state,new HashMap<>());
        transitions.put(key,new_state);
        transition_matrix.put(state, transitions);
        Map<Character,String> outputs = output_matrix.getOrDefault(state,new HashMap<>());
        outputs.put(key,result);
        output_matrix.put(state, outputs);
    }
}
