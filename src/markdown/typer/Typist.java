package markdown.typer;

public abstract class Typist {

    private String buffer;

    public Typist(String _buffer){
        buffer = _buffer;
    }

    public void clear(){
        buffer = "";
    }

    public Typist(){
         buffer = "";
    }

    public void addBuffer(char ch){
        buffer = buffer.concat(String.valueOf(ch));
    }

    protected String getBuffer(){
        return buffer;
    }

    public abstract String result();
    public abstract Typist close(char ch);
    public abstract Typist update(char ch);
}
