package markdown.typer.grating;

import markdown.typer.Typist;

public class Grating_Text extends Typist {
    @Override
    public String result() {
        String result = "<h1>"+ getBuffer()+"</h1>";
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        addBuffer(ch);
        return this;
    }

    @Override
    public Typist update(char ch) {
        String buffer = getBuffer();
        clear();
        return new Grating_Grating_Text(buffer);
    }
}
