package markdown.typer.grating;

import markdown.typer.Typist;

public class Grating_Grating_Text extends Typist {

    public Grating_Grating_Text(String buffer){
        super(buffer);
    }

    @Override
    public String result() {
        String result = "<h2>" + getBuffer() + "</h2>";
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        addBuffer(ch);
        return this;
    }

    @Override
    public Typist update(char ch) {
        addBuffer(ch);
        return this;
    }
}
