package markdown.typer;

public class Text extends Typist{

    public Text(){}

    @Override
    public String result() {
        String result = getBuffer();
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        addBuffer(ch);
        return this;
    }

    @Override
    public Typist update(char ch) {
        addBuffer(ch);
        return this;
    }
}
