package markdown.typer;

public class TypistList extends Typist {

    private String new_buffer;

    public TypistList(){
        new_buffer="";
    }

    @Override
    public String result() {
        update('\n');
        clear();
        String result = "<ul>\n" + new_buffer + "</ul>\n";
        new_buffer = "";
        return result;
    }

    @Override
    public Typist close(char ch) {
        addBuffer(ch);
        return this;
    }

    @Override
    public Typist update(char ch) {
        new_buffer = new_buffer + "<li>" + getBuffer() + "</li>"+ String.valueOf(ch);
        clear();
        return this;
    }
}
