package markdown.typer.star;

import markdown.typer.Typist;

public class Star_Star_Text_Star_Star extends Typist {

    public Star_Star_Text_Star_Star(String buffer){
        super(buffer);
    }

    @Override
    public String result() {
        String result = format(getBuffer());
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        addBuffer(ch);
        return this;
    }

    @Override
    public Typist update(char ch) {
        addBuffer(ch);
        return this;
    }

    public static String format(String text){
        return "<b>" + text + "</b>";
    }
}
