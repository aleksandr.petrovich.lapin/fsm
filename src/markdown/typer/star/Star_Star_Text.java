package markdown.typer.star;

import markdown.typer.Typist;

public class Star_Star_Text extends Typist {

    public Star_Star_Text(String buffer){
        super(buffer);
    }

    @Override
    public String result() {
        String result = format(getBuffer());
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        String buffer = getBuffer();
        clear();
        return new Star_Star_Text_Star(buffer);
    }

    @Override
    public Typist update(char ch) {
        addBuffer(ch);
        return this;
    }

    public static String format(String text){
        return Star_Text.format(Star_Text.format(text));
    }
}
