package markdown.typer.star;

import markdown.typer.Typist;

public class Star_Text extends Typist {

    @Override
    public String result() {
        String result = "*" + getBuffer();
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        String buffer = getBuffer();
        clear();
        return new Star_Text_Star(buffer);
    }

    @Override
    public Typist update(char ch) {
        String buffer = getBuffer();
        clear();
        return new Star_Star_Text(buffer);
    }

    public static String format(String text){
        return "*" + text;
    }
}
