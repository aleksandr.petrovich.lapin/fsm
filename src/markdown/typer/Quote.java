package markdown.typer;

public class Quote extends Typist {

    @Override
    public String result() {
        String result = "<blockquote>" + getBuffer() + "</blockquote>";
        clear();
        return result;
    }

    @Override
    public Typist close(char ch) {
        addBuffer(ch);
        return this;
    }

    @Override
    public Typist update(char ch) {
        addBuffer(ch);
        return this;
    }
}
