package markdown;

import markdown.typer.Text;
import markdown.typer.Typist;

public class Hoarder {
    private String result;
    private Typist typist;

    public Hoarder(){
        result = "";
        typist = new Text();
    }

    public void addTypist(Typist _typist){
        result = result + typist.result();
        typist = _typist;
    }

    public void addChar(char ch){
        typist.addBuffer(ch);
    }

    public void updateTypist(char ch){
        typist = typist.update(ch);
    }

    public void closeTypist(char ch){
        typist = typist.close(ch);
    }

    public String getResult(){
        return result + typist.result();
    }
}
