package abstract_automaton;

public class UnknownStartState extends ExceptionFSM {
    public UnknownStartState(){
        super("Start state is null");
    }
}
