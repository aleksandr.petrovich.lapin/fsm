package abstract_automaton;

public class UnknownState extends ExceptionFSM {
    public UnknownState(){
        super("The automation must be in a state that has not been determined");
    }
}
