package abstract_automaton;

public class UncutTransition extends ExceptionFSM {
    public UncutTransition(){
        super("In the current state, the transition with the received key was not determined");
    }
}
