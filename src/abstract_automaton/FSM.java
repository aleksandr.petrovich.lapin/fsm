package abstract_automaton;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FSM<Input, State, Output> {
    private State start_state;
    private Map<State,Map<Input,State>> transition_matrix;
    private Map<State,Map<Input,Output>> output_matrix;

    public FSM(){
        start_state = null;
        transition_matrix = new HashMap<>();
        output_matrix = new HashMap<>();
    }

    public FSM(Map<State,Map<Input,State>> _transition_matrix, Map<State,Map<Input,Output>> _output_matrix){
        transition_matrix = _transition_matrix;
        output_matrix = _output_matrix;
    }

    private State transition(State now_state,Input input_value){
        return transition_matrix.get(now_state).get(input_value);
    }

    private Output output(final State now_state,Input input_value) throws ExceptionFSM {
        Map<Input, Output> outputs = output_matrix.get(now_state);
        if (outputs == null){
            throw new UnknownState();
        }
        Output result = outputs.get(input_value);
        if (result == null){
            throw new UncutTransition();
        }
        return result;
    }

    public void addStartState(State state){
        start_state = state;
    }

    public void addNewTransition(State state, Input key, State new_state, Output result){
        Map<Input,State> transitions = transition_matrix.getOrDefault(state,new HashMap<>());
        transitions.put(key,new_state);
        transition_matrix.put(state, transitions);
        Map<Input,Output> outputs = output_matrix.getOrDefault(state,new HashMap<>());
        outputs.put(key,result);
        output_matrix.put(state, outputs);
    }

    public final List<Output> readCommands(Input[] input_data) throws ExceptionFSM {
        if (start_state == null){
            throw new UnknownStartState();
        }
        State state = start_state;
        List<Output> output_data = new ArrayList<>();
        for (Input input: input_data){
            output_data.add(output(state,input));
            state = transition(state, input);
        }
        return output_data;
    }
}
