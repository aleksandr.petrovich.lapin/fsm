package abstract_automaton;

public class ExceptionFSM extends Exception{
    public ExceptionFSM(String message){
        super(message);
    }
}
