package first_fsm;

import abstract_automaton.FSM;

public class FirstFSM extends FSM<Integer,String, String> {
    public FirstFSM() {
        super();
        String[] stats = {"q1", "q2", "q3"};
        Integer[] keys = {0, 1};
        addStartState(stats[0]);
        addNewTransition(stats[0],keys[0], stats[0], getResult(stats[0],stats[0],keys[0]));
        addNewTransition(stats[0],keys[1], stats[1], getResult(stats[0],stats[1],keys[1]));
        addNewTransition(stats[1],keys[0], stats[2], getResult(stats[1],stats[2],keys[0]));
        addNewTransition(stats[1],keys[1], stats[1], getResult(stats[1],stats[1],keys[1]));
        addNewTransition(stats[2],keys[0], stats[1], getResult(stats[2],stats[1],keys[0]));
        addNewTransition(stats[2],keys[1], stats[1], getResult(stats[2],stats[1],keys[1]));
    }

    private static String getResult(String state1, String state2, Integer key){
        return key.toString() + ": " + state1 + " -> " + state2;
    }
}
