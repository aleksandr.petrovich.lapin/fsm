package first_fsm;

import abstract_automaton.ExceptionFSM;
import abstract_test.Test;

import java.util.ArrayList;
import java.util.List;

public class TestFirstFSM {

    public static void main(String[] args) {
        List<Test> tests = new ArrayList<>();

        tests.add(new Test("Test1FirstFEM") {
            public boolean test() {
                boolean result;
                try {
                    FirstFSM fsm = new FirstFSM();
                    List<String> result_list = fsm.readCommands(new Integer[]{1,0,0,1,0});

                    result =
                            5 == result_list.size() &&
                            result_list.get(0).equals("1: q1 -> q2") &&
                            result_list.get(1).equals("0: q2 -> q3") &&
                            result_list.get(2).equals("0: q3 -> q2") &&
                            result_list.get(3).equals("1: q2 -> q2") &&
                            result_list.get(4).equals("0: q2 -> q3");
                } catch (ExceptionFSM exceptionFSM) {
                    result = false;
                    exceptionFSM.printStackTrace();
                }
                return result;
            }
        });


        for (Test test: tests) {
            System.out.println(test);
        }
    }
}
